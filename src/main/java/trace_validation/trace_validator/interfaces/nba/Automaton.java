/**
 * Generated by quicktype.io with the JSON Schema
 */

package trace_validation.trace_validator.interfaces.nba;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Automaton {
    private List<State> states;
    private List<List<Transition>> transitions;

    @JsonProperty("states")
    public List<State> getStates() { return states; }
    @JsonProperty("states")
    public void setStates(List<State> value) { this.states = value; }

    @JsonProperty("transitions")
    public List<List<Transition>> getTransitions() { return transitions; }
    @JsonProperty("transitions")
    public void setTransitions(List<List<Transition>> value) { this.transitions = value; }
    
    /* Extension of the automatically generated class below */
    
    public State getState(String id) {
    	for (State state : states) {
    		if (state.getID().contentEquals(id)) return state;
    	}
    	return null;
    }
}
