/**
 * Generated by quicktype.io with the JSON Schema
 */

package trace_validation.trace_validator.interfaces.nba;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Nba {
    private Automaton automaton;
    private String name;
    private List<String> split;
    private List<Where> where;

    @JsonProperty("automaton")
    public Automaton getAutomaton() { return automaton; }
    @JsonProperty("automaton")
    public void setAutomaton(Automaton value) { this.automaton = value; }

    /**
     * Name to describe the requirement
     */
    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    /**
     * Optional indication on where to split iterations
     */
    @JsonProperty("split")
    public List<String> getSplit() { return split; }
    @JsonProperty("split")
    public void setSplit(List<String> value) { this.split = value; }

    /**
     * Relation between events in the requirement and events in the trace
     */
    @JsonProperty("where")
    public List<Where> getWhere() { return where; }
    @JsonProperty("where")
    public void setWhere(List<Where> value) { this.where = value; }
}
