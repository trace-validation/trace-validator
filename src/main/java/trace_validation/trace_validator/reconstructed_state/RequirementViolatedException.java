package trace_validation.trace_validator.reconstructed_state;

public class RequirementViolatedException extends Exception {
	private static final long serialVersionUID = 1L;

	public RequirementViolatedException(String s) {
		super(s);
	}
}
