package trace_validation.trace_validator.reconstructed_state;

import java.util.*;

import org.eclipse.tracecompass.ctf.core.event.IEventDefinition;

import trace_validation.trace_validator.interfaces.nba.Where;

/** Holds the state that has been reconstructed with the trace */
public class ReconstructedState {

	Set<Proposition> state;
	
	public ReconstructedState(List<Where> translations) {
		state = new HashSet<>();
		
		for (Where ap : translations) {
			Proposition prop = new Proposition(ap.getEvent());
			Translation trans = new Translation(ap);
			prop.setTranslation(trans);
			state.add(prop);
		}
	}
	
	public void update(IEventDefinition event) {
		for (Proposition prop : state) {
			prop.update(event);
		}
	}

	public Set<Proposition> getState() {
		return state;
	}

	public void setState(Set<Proposition> state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		List<String> apStatus = new ArrayList<>();
		
		for (Proposition ap : state) {
			apStatus.add((ap.isActive() ? "" : "!") + ap.getId());
		}
		
		return "{" + String.join(", ", apStatus) + "}";
	}
}
