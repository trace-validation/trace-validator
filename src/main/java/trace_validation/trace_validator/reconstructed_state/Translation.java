package trace_validation.trace_validator.reconstructed_state;

import java.util.*;

import trace_validation.trace_validator.interfaces.nba.Where;

class Translation {
	private String 			onTraceEvent;
	private List<String> 	onTraceParams;
	private String 			offTraceEvent;
	private List<String> 	offTraceParams;

	public Translation(Where where) {
		this.onTraceEvent = where.getOn();
		this.offTraceEvent = where.getOff();
		this.onTraceParams = new ArrayList<>();
		this.offTraceParams = new ArrayList<>();
		
		if (where.getOnParameters() != null) {
			for (String onParam : where.getOnParameters()) {
				onTraceParams.add(onParam);
			}
		}
		
		if (where.getOffParameters() != null) {
			for (String offParam : where.getOffParameters()) {
				offTraceParams.add(offParam);
			}
		}
	}
	
	public String getOnTraceEvent() {
		return onTraceEvent;
	}

	public void setOnTraceEvent(String onTraceEvent) {
		this.onTraceEvent = onTraceEvent;
	}

	public List<String> getOnTraceParams() {
		return onTraceParams;
	}

	public void setOnTraceParams(List<String> onTraceParams) {
		this.onTraceParams = onTraceParams;
	}

	public String getOffTraceEvent() {
		return offTraceEvent;
	}

	public void setOffTraceEvent(String offTraceEvent) {
		this.offTraceEvent = offTraceEvent;
	}

	public List<String> getOffTraceParams() {
		return offTraceParams;
	}

	public void setOffTraceParams(List<String> offTraceParams) {
		this.offTraceParams = offTraceParams;
	}
	
}
