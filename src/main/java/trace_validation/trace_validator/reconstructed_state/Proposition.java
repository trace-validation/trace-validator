package trace_validation.trace_validator.reconstructed_state;

import org.eclipse.tracecompass.ctf.core.event.IEventDefinition;

/** Contains the state of a single element of the reconstructed system state. */
public class Proposition {
	private String id;
	private Translation translation;
	private boolean isActive;
	
	public Proposition(String id) {
		this.id = id;
		isActive = false;
		
		if (id.contentEquals("true")) isActive = true;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isActive() {
		return isActive;
	}

	public void activate() {
		this.isActive = true;
	}
	
	public void deactivate() {
		this.isActive = false;
	}
	
	public Translation getTranslation() {
		return translation;
	}

	public void setTranslation(Translation translation) {
		this.translation = translation;
	}

	public void update(IEventDefinition event) {
		/*
		 * First check off. This allows for 'off' being a superset of the 'on' 
		 * condition s.t. the proposition will be activated.
		 * This way around is more desirable as 'on' conditions are more likely
		 * precisely defined, whereas off might be 'anything else'.
		 * The inverse can still be achieved by using the negation of the AP.
		 */
		
		/* Check for off */
		if (event.getDeclaration().getName().contentEquals(
				translation.getOffTraceEvent())) {
			// Check parameters
			boolean parametersMatch = true;
			int paramIdx = 0;
			
			for (String paramName : event.getFields().getFieldNames()) {
				String paramValue = event.getFields().getDefinition(paramName).toString();
				// Strings still have quotes around them, remove those
				paramValue = paramValue.replaceAll("\"", "");
				String desiredValue = translation.getOffTraceParams().get(paramIdx++);
				if(desiredValue != null && !desiredValue.contentEquals(paramValue)) {
					parametersMatch = false;
					break;
				}
			}
			
			if (parametersMatch) deactivate();
		}
		/* Check for on */
		if (event.getDeclaration().getName().contentEquals(
				translation.getOnTraceEvent())) {
			// Check parameters
			boolean parametersMatch = true;
			int paramIdx = 0;
			
			for (String paramName : event.getFields().getFieldNames()) {
				String paramValue = event.getFields().getDefinition(paramName).toString();
				// Strings still have quotes around them, remove those
				paramValue = paramValue.replaceAll("\"", "");
				String desiredValue = translation.getOnTraceParams().get(paramIdx++);
				
				if(desiredValue != null && !desiredValue.contentEquals(paramValue)) {
					parametersMatch = false;
					break;
				}
			}
			
			if (parametersMatch) activate();
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (! (o instanceof Proposition)) return false;
		
		Proposition prop = (Proposition) o;
		return id.contentEquals(prop.getId()) && isActive == prop.isActive();
	}
	
	@Override
	public String toString() {
		return isActive ? id : ("!" + id);
	}

}
