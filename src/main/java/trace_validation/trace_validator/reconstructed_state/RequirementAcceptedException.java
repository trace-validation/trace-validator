package trace_validation.trace_validator.reconstructed_state;

public class RequirementAcceptedException extends Exception {

	private static final long serialVersionUID = 1L;

	public RequirementAcceptedException(String s) {
		super(s);
	}

}
