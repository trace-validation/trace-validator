package trace_validation.trace_validator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.tracecompass.ctf.core.CTFException;
import org.eclipse.tracecompass.ctf.core.trace.CTFTrace;
import org.eclipse.tracecompass.ctf.core.trace.CTFTraceReader;

import trace_validation.trace_validator.interfaces.nba.*;
import trace_validation.trace_validator.reconstructed_state.ReconstructedState;
import trace_validation.trace_validator.reconstructed_state.RequirementAcceptedException;
import trace_validation.trace_validator.reconstructed_state.RequirementViolatedException;
import trace_validation.trace_validator.simulated_automaton.SimulatedAutomaton;

public class Validator {
	
	/** The automaton that holds the NBA for verification */
	static SimulatedAutomaton automaton;
	static SimulatedAutomaton negAutomaton;
	
	/** The reconstructed state based on the traces */
	static ReconstructedState reconstructedState;
	
	/** Reader for trace inputs */
	static CTFTraceReader traceReader;
	
	public static void main(String args[]) {
		// Very simple input validation
		if (args.length != 3) {
			System.out.println("Error, incorrect number of arguments.");
			System.out.println("Argument 1: Requirement NBA");
			System.out.println("Argument 2: Negated Requirement NBA");
			System.out.println("Argument 3: Trace in CTF format");
			System.exit(1);
		}
		
		/* Read requirement */
		Nba requirement = createRequirement(args[0]);
		Nba negRequirement = createRequirement(args[1]);
		
		/* Construct requirement automaton */
		automaton = new SimulatedAutomaton(requirement, false);
		negAutomaton = new SimulatedAutomaton(negRequirement, true);
		
		/* Create empty reconstructed state */
		reconstructedState = new ReconstructedState(requirement.getWhere());
		
		/* Load trace */
		try {
			CTFTrace trace = new CTFTrace(args[2]);
			traceReader = new CTFTraceReader(trace);
		} catch(Exception e) {
			System.err.println(e);
			System.exit(1);
		}
		
		/* Run verification algorithm */
		try {
			while(traceReader.hasMoreEvents()) {
				// Update system
				reconstructedState.update(traceReader.getCurrentEventDef());
				
				// Transition on the updated reconstructedState
				automaton.execute(reconstructedState);
				negAutomaton.execute(reconstructedState);
				
				// Go to the next event
				if (!traceReader.advance()) break;
			}

			// Validate 
			automaton.validate();
			negAutomaton.validate();
			
		} catch (RequirementViolatedException e) {
			System.out.println("The requirement was violated\n" + e);
			// TODO print automaton state
//			System.out.println(automaton);
			return;
		} catch (RequirementAcceptedException e) {
			System.out.println("The requirement was satisfied\n" + e);
			return;
		} catch (CTFException e) {
			System.err.println("An error in the trace occurred: " + e);
			System.exit(1);
		}
		
		traceReader.close();
		
		/* Succeeded */
		System.out.println("No violation or satisfaction was reached.\n" + 
				"This may mean different things:\n" +
				"- The trace did not provide evidence for a decision\n" +
				"- The requirement is undecible on a finite trace.");
	}
	
	private static String readFile(String file) throws IOException {
	    BufferedReader reader = new BufferedReader(new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    try {
	        while((line = reader.readLine()) != null) {
	            stringBuilder.append(line);
	            stringBuilder.append(ls);
	        }

	        return stringBuilder.toString();
	    } finally {
	        reader.close();
	    }
	}
	
	private static Nba createRequirement(String requirementFile) {
		Nba requirement = null;
		try {
			String requirementText = readFile(requirementFile);
			requirement = NbaParser.fromJsonString(requirementText);
		} catch (Exception e) {
			System.err.println(e);
			System.exit(1);
		}
		return requirement;
	}
}
