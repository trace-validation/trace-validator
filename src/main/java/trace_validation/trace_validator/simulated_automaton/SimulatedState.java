package trace_validation.trace_validator.simulated_automaton;

import java.util.*;

import trace_validation.trace_validator.reconstructed_state.ReconstructedState;

public class SimulatedState {
	private String id;
	private Set<SimulatedTransition> outgoingTransitions;
	private boolean isAccepting;

	public SimulatedState(String id, boolean isAccepting) {
		this.id = id;
		this.isAccepting = isAccepting;
		this.outgoingTransitions = new HashSet<>();
	}
	
	protected Set<SimulatedState> execute(ReconstructedState systemState) {
		Set<SimulatedState> nextStates = new HashSet<>();
		
		for (SimulatedTransition transition : outgoingTransitions) {
			if (transition.isValid(systemState)) {
				nextStates.add(transition.getDestination());
			}
		}
		
		return nextStates;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<SimulatedTransition> getOutgoingTransitions() {
		return outgoingTransitions;
	}

	public void setOutgoingTransitions(Set<SimulatedTransition> outgoingTransitions) {
		this.outgoingTransitions = outgoingTransitions;
	}

	public boolean isAccepting() {
		return isAccepting;
	}

	public void setAccepting(boolean isAccepting) {
		this.isAccepting = isAccepting;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (! (o instanceof SimulatedState)) return false;
		
		SimulatedState state = (SimulatedState) o;
		
		return id.contentEquals(state.getId());
	}
	
}
