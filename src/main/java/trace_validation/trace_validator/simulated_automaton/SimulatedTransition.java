package trace_validation.trace_validator.simulated_automaton;

import java.util.*;

import trace_validation.trace_validator.reconstructed_state.Proposition;
import trace_validation.trace_validator.reconstructed_state.ReconstructedState;

public class SimulatedTransition {
	private SimulatedState source;
	private Set<Proposition> label;
	private SimulatedState destination;

	public SimulatedTransition() {}
	
	public SimulatedTransition(SimulatedState src, 
			Set<Proposition> labels, 
			SimulatedState dst) {
		source = src;
		label = labels;
		destination = dst;
	}
	
	public SimulatedState getSource() {
		return source;
	}

	public void setSource(SimulatedState source) {
		this.source = source;
	}

	public Set<Proposition> getLabel() {
		return label;
	}

	public void setLabel(Set<Proposition> label) {
		this.label = label;
	}

	public SimulatedState getDestination() {
		return destination;
	}

	public void setDestination(SimulatedState destination) {
		this.destination = destination;
	}

	/** Check if this transition is activated with the given system state */
	protected boolean isValid(ReconstructedState systemState) {
		// Check if all elements of label are true in systemState
		for (Proposition labelElement : label) {
			for (Proposition stateElement : systemState.getState()) {
				if (labelElement.getId().equals(stateElement.getId())) {
					if (!labelElement.equals(stateElement)) {
						return false;
					}
				}
			}
		}
		
		return true;
	}

}
