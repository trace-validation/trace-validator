package trace_validation.trace_validator.simulated_automaton;

import java.util.*;

import trace_validation.trace_validator.interfaces.nba.*;
import trace_validation.trace_validator.reconstructed_state.Proposition;
import trace_validation.trace_validator.reconstructed_state.ReconstructedState;
import trace_validation.trace_validator.reconstructed_state.RequirementAcceptedException;
import trace_validation.trace_validator.reconstructed_state.RequirementViolatedException;

public class SimulatedAutomaton {
	private Set<SimulatedState> currentStates;
	boolean hasAcceptingStates;
	boolean negated;

	public SimulatedAutomaton(Nba nba, boolean negated) {
		this.negated = negated;
		
		currentStates = new HashSet<>();
		Set<SimulatedState> states = new HashSet<>();
		Set<String> aps = getAPs(nba);
		
		for (List<Transition> transition : nba.getAutomaton().getTransitions()) {
			String source = transition.get(0).stringValue;
			SimulatedState sourceState = null;
			
			String dest = transition.get(2).stringValue;
			SimulatedState destState = null;
			
			// Match states to existing states
			for (SimulatedState state : states) {
				if (state.getId().contentEquals(source)) sourceState = state;
				if (state.getId().contentEquals(dest)) destState = state;
			}
			
			// If a state was not yet created, do so now
			if (sourceState == null) {
				State sourceNBAState = nba.getAutomaton().getState(source);
				sourceState = new SimulatedState(source, 
						sourceNBAState.getAccepting());
				states.add(sourceState);
				if (sourceNBAState.getInitial()) currentStates.add(sourceState);
				if (sourceNBAState.getAccepting()) hasAcceptingStates = true;
			}
			// If src and dest are the same, do not create it twice
			if (source.contentEquals(dest)) destState = sourceState;
			// Try to create destination state if necessary
			if (destState == null) {
				State dstNBAState = nba.getAutomaton().getState(dest);
				destState = new SimulatedState(dest, 
						dstNBAState.getAccepting());
				states.add(destState);
				if (dstNBAState.getInitial()) currentStates.add(destState);
				if (dstNBAState.getAccepting()) hasAcceptingStates = true;
			}

			// Translate labels
			List<String> labels = transition.get(1).stringArrayValue;
			Set<Proposition> newLabels = new HashSet<>();
			for (String label : aps) {
				Proposition prop = new Proposition(label);
				if (labels.contains(label)) prop.activate();
				newLabels.add(prop);
			}
			
			// Finally, create transition
			SimulatedTransition st = new SimulatedTransition(
					sourceState, newLabels, destState);
			sourceState.getOutgoingTransitions().add(st);
		}
	}
	
	/** Make one step in the automaton */
	public void execute(ReconstructedState systemState) throws RequirementViolatedException, RequirementAcceptedException {
		Set<SimulatedState> nextStates = new HashSet<>();
		
		for (SimulatedState state : currentStates) {
			nextStates.addAll(state.execute(systemState));
		}
		
		currentStates = nextStates;
		if (currentStates.isEmpty()) {
			if (negated) {
				throw new RequirementAcceptedException("When executing the trace, "
						+ "there were no more current states.\n"
						+ "For the negated NBA, this means that the requirement "
						+ "must be satisfied.");
			} else {
				throw new RequirementViolatedException("When executing the trace, "
						+ "there were no more current states.\n"
						+ "For the positive NBA, this means that the requirement "
						+ "must be violated.");	
			}
		}
	}
	
	/** Validate the requirement (at the end of the trace) */
	public void validate() throws RequirementAcceptedException, RequirementViolatedException {
		/* Check for loop containing current state and accepting state */
		for (SimulatedState currentState : currentStates) {
			EmptinessChecker ec = new EmptinessChecker();
			if (ec.checkEmptiness(currentState, hasAcceptingStates)) {
				// If not such a loop
				if (negated) {
					throw new RequirementAcceptedException("Cannot visit infinitely many accepting states");
				} else {
					throw new RequirementViolatedException("Cannot visit inifinitely many accepting states");
				}
			}
		}
	}
	
	private class EmptinessChecker {
		/** Relative counter for discovery time */
		private int time = 0;
		
		private Set<EmptinessState> visited;
		private Stack<EmptinessState> candidate;
		private Stack<EmptinessState> active;
		
		public boolean checkEmptiness(SimulatedState currentState, 
				boolean mustBeAccepting) {
			visited = new HashSet<>();
			candidate = new Stack<>();
			active = new Stack<>();
			
			return dfs(currentState, mustBeAccepting);			
		}
		
		/** 
		 * Depth first search of the graph for accepting cycles, starting in
		 * currentState. Returns false (i.e. non-empty) iff such a cycle exists. 
		 */
		private boolean dfs(SimulatedState currentState, boolean mustBeAccepting) {
			EmptinessState currentES = new EmptinessState(currentState, getTime());
			visited.add(currentES);
			candidate.push(currentES);
			active.push(currentES);
			
			for (SimulatedTransition transition : currentState.getOutgoingTransitions()) {
				SimulatedState nextState = transition.getDestination();
				
				if (!isInVisited(nextState)) {
					if (!dfs(nextState, mustBeAccepting)) {
						return false;
					}
				} else if(isInActive(nextState)) {
					EmptinessState s;
					int nextStateDiscoveryTime = getDiscoveryTime(nextState);
					do {
						s = candidate.pop();
						if (s.getState().isAccepting() || !mustBeAccepting) {
							return false;
						}
					} while(s.discoveryTime > nextStateDiscoveryTime);
					candidate.push(s);
				} else {
					// Nothing
				}
			}
			if (candidate.peek().equals(currentState)) {
				candidate.pop();
				EmptinessState s;
				do {
					s = active.pop();
				} while(!s.equals(currentState));
			}
			
			return true;
		}

		/** .contains() doesn't work as .equals() of BAState and EmptinessState 
		 * is asymmetric */
		private boolean isInVisited(SimulatedState state) {
			for (EmptinessState es : visited) {
				if (es.equals(state)) return true;
			}
			return false;
		}
		
		/** .contains() doesn't work as .equals() of BAState and EmptinessState 
		 * is asymmetric */
		private boolean isInActive(SimulatedState state) {
			for (EmptinessState es : active) {
				if (es.equals(state)) return true;
			}
			return false;
		}
		
		protected int getTime() {
			return time++;
		}
		
		private int getDiscoveryTime(SimulatedState state) {
			for (EmptinessState es : visited) {
				if (es.equals(state)) return es.discoveryTime;
			}
			return -1;
		}
		
		private class EmptinessState {
			private SimulatedState state;
			private int discoveryTime;
			
			public EmptinessState(SimulatedState state, int time) {
				this.state = state;
				this.discoveryTime = time;
			}
			
			@Override
			public boolean equals(Object o) {
		    	if (o == this) return true;
		    	if (o == null) return false;
		    	
		    	if (o instanceof SimulatedState) {
		    		SimulatedState object = (SimulatedState) o;
		    		return object.equals(this.state);
		    	}
		    	
		    	if (o instanceof EmptinessState) {
		    		EmptinessState object = (EmptinessState) o;
		    		return object.state.equals(this.state);
		    	}
		    	
		    	return false;
			}

			public SimulatedState getState() {
				return state;
			}
			
		}
	}

	public Set<SimulatedState> getCurrentStates() {
		return currentStates;
	}

	public void setCurrentStates(Set<SimulatedState> currentStates) {
		this.currentStates = currentStates;
	}
	
	private Set<String> getAPs(Nba nba) {
		Set<String> aps = new HashSet<>();
		
		for (Where where : nba.getWhere()) {
			aps.add(where.getEvent());
		}

		// Check if 'true' is present in this requirement
		for (List<Transition> trans : nba.getAutomaton().getTransitions()) {
			boolean hasTrueLabel = false;
			for (String label : trans.get(1).stringArrayValue) {
				if (label.contentEquals("true")) {
					hasTrueLabel = true;
				}
			}
			if (hasTrueLabel) {
				aps.add("true");
				break;
			}
		}
		
		return aps;
	}
	
	@Override
	public String toString() {
		List<String> currentStateNames = new ArrayList<>();
	
		for (SimulatedState state : currentStates) {
			currentStateNames.add(state.getId());
		}
		
		return "{" + String.join(", ", currentStateNames) + "}";
	}
	
}
