#include <stdio.h>
#include "hello-tp.h"

void a(char);
void b(char);
void c(char);

int main(int argc, char *argv[])
{
	a(1);
	b(1);
	a(1);
	b(1);
	a(0);
	b(0);
	b(0);
    return 0;
}

void a(char on) {
	tracepoint(hw, barectf_ust_a, 42, on ? "start" : "finish");
}

void b(char on) {
	if (on) {
		tracepoint(hw, barectf_on_event, 1);
	} else {
		tracepoint(hw, barectf_off_event, 1);
	}
}

void c(char on) {
	if (on) {
		tracepoint(hw, c_on);
	} else {
		tracepoint(hw, c_off);
	}	
}