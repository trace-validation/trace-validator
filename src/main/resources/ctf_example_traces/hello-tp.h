#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER hw

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "./hello-tp.h"

#if !defined(_HELLO_TP_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _HELLO_TP_H

#include <lttng/tracepoint.h>

TRACEPOINT_EVENT(
	hw,
	barectf_ust_a,
	TP_ARGS(
		int, integer_arg,
		char*, string_arg
	),
	TP_FIELDS(
		ctf_integer(int, integer, integer_arg)
		ctf_string(string, string_arg)
	)
)

TRACEPOINT_EVENT(
	hw,
	barectf_on_event,
	TP_ARGS(
		int, integer_arg
	),
	TP_FIELDS(
		ctf_integer(int, integer, integer_arg)
	)
)

TRACEPOINT_EVENT(
	hw,
	barectf_off_event,
	TP_ARGS(
		int, integer_arg
	),
	TP_FIELDS(
		ctf_integer(int, integer, integer_arg)
	)
)

TRACEPOINT_EVENT(
	hw,
	c_on,
	TP_ARGS(),
	TP_FIELDS()
)

TRACEPOINT_EVENT(
	hw,
	c_off,
	TP_ARGS(),
	TP_FIELDS()
)

#endif /* _HELLO_TP_H */

#include <lttng/tracepoint-event.h>
