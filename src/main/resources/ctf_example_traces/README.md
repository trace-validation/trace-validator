# Hello World traces

This folder contains a set of manually created traces to test `(a || b) U c`.

## Trace translations
**a on** `hw:barectf_ust_a(null, "start")`

**a off** `hw:barectf_ust_a(null, "finish")`

**b on** `hw:barectf_on_event(1)`

**b off** `hw:barectf_off_event(1)`

**c on** `hw:c_on()`

**c off** `hw:c_off()`

## Accepting traces
**a1** `[c on]`

**a2** `[a on; c on; a off]`

**a3** `[a on; b on; a off; c on; c off; b off]`

**a4** `[b on; b on; a on; a on; b on; b off; a on; c on]`

**a5** `[a on; b on; b off; b off; c on]`

**a6** `[]`

Note that an empty trace is undecidable in LTL3


## Violating traces
**v1** `[a on; a off]`

**v2** `[a on; b on; a on; b on; a off; b off; b off]`
