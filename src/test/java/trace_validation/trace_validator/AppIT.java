package trace_validation.trace_validator;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppIT {

	ByteArrayOutputStream output = null;
	
	@Before
	public void setUpIO() {
		// Redirect System.out
		output = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(output);
		System.setOut(ps);
	}
	
	/**
	 * Tests if the output equals the expected output.
	 */
	@Test
	public void testOutput() {
		// relative path to pom.xml
		final String path = "src/main/resources/ctf_example_traces/";
		final String nbaPath = path + "ltl_simple.nba.json";
		final String negNbaPath = path + "neg_ltl_simple.nba.json";
		final String[] traces = {"a1/", "a2/", "a3/", "a4/", "a5/", "a6/", "v1/", "v2/"};

		// run the SUT
		for (int i = 0; i < 8; ++i) {
			final String[] args = {nbaPath, negNbaPath, path + traces[i] + "ust/uid/1000/64-bit"};
			Validator.main(args);
			
			// Forward output
			System.err.println(output.toString());
			
			// Determine correctness
			if (i < 5) { 
				// Expect acceptance
				assertTrue("Trace " + i + " should have been satisfied, but was not",
						output.toString().contains("trace_validation.trace_validator.reconstructed_state.RequirementAcceptedException"));
			} else if (i == 5) {
				// Expect undecidable
				assertTrue("Trace " + i + " should have been undecidable, but was not",
						output.toString().contains("No violation or satisfaction was reached."));
			} else { 
				// Expect violation
				assertTrue("Trace " + i + " should have been violated, but was not",
						output.toString().contains("trace_validation.trace_validator.reconstructed_state.RequirementViolatedException"));
			}
		}
	}
	
}
