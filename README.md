# Trace validator

This project reads a CTF trace, and runs the trace on the Büchi automaton from the [Requirement Converter](https://gitlab.com/trace-validation/requirement-converter) project.

## Reading Büchi automaton
This project uses the same JSON schema as the Requirement Converter project to generated code through [quicktype.io](https://app.quicktype.io/).

## Reading traces
Reading CTF traces uses parts of Trace Compass. These jars are added to this project as they are not available in any Maven repository. They were obtained by using [a tool on GitHub](https://github.com/frobino/ctf_trace_apps/tree/master/Java/ctf2json).

## State reconstruction
The Büchi automaton takes the whole system state (all abstract propositions) as input, and transitions based on the truth value of each AP. As the trace (generally) does not provide an update of all APs at each event, a workaround is used.

The state of the original program is [reconstructed](https://gitlab.com/trace-validation/trace-validator/-/tree/master/src/main/java/trace_validation/trace_validator/reconstructed_state) based on the incoming trace events. Each event can set an AP to true or false, as specified in the original requirement JSON.

All APs start out as `false`, and after each trace event one step in the automaton is taken.

## Simulation
With the reconstructed state, the [Büchi automaton](https://gitlab.com/trace-validation/trace-validator/-/tree/master/src/main/java/trace_validation/trace_validator/simulated_automaton) is executed. Since the Büchi automaton is the negation of the original requirement, the requirement is violated by the trace iff the Büchi automaton accepts the input. If the automaton rejects the input, the requirement may be not be violated or that the requirement is undecidable on a finite trace.

Note that acceptance of the trace means that the particular input trace did not violate the requirement. This does not mean that no trace can violate the requirement.

### Split
Split can be used to gain confidence in undecidable requirements. By splitting the trace in many parts, requirements can be validated on each part. If none of the parts is violated (but all are undecidable), one might say with increased confidence that the requirement is satisfied.

To use this, the automaton needs to be able to destinguish between violated, non-violated and undecidable requirements. As this was developed for the FSM, and the Büchi automaton cannot do this, the master branch does not handle splits. The `split` branch does handle this.

## Requirements
* JDK (developed for 8.0)
* Apache Maven
* 2 JSON Büchi automata
* CTF trace

## Usage
1. `git clone https://gitlab.com/trace-validation/trace-validator`
2. `cd trace-validator`
3. `mvn initialize`
4. `mvn install`
5. `cd target`
6. `java -jar trace-validater-0.0.1-SNAPSHOT-jar-with-dependencies.jar <NBA JSON> <Negated NBA JSON> <CTF Trace>`

### Example
This project has an example requirement and several traces to validate. Run this as follows for step 5 above.

`java -jar trace-validater-0.0.1-SNAPSHOT-jar-with-dependencies.jar ../src/main/resources/nba_example.json ../src/main/resources/neg_nba_example.json ../src/main/resources/ctf_example_traces/a1/ust/uid/1000/64-bit`

Similarly the other traces can be run. `a1` through `a5` should be accepting. `v1`, `v2` and `v3` violate the requirement. See `README.md` in the `ctf_example_traces` directory for a description of the traces.
